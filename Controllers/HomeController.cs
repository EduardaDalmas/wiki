﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using DsEad.Models;
using DsEad.Data;

namespace DsEad.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly appWikiContext _context;


        public HomeController(ILogger<HomeController> logger, appWikiContext context)
        {
            _logger = logger;
            _context = context;
        }

        public IActionResult Index()
        {
            IList<Artigo> artigos = _context.Artigos.ToList();
            return View(artigos);
        }

        public IActionResult Artigo(int? id)
        {
            Artigo artigo = _context.Artigos.Where(a => a.Id == id).FirstOrDefault();
            return View(artigo);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
