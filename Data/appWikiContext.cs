﻿using System;
using DsEad.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace DsEad.Data
{
    public partial class appWikiContext : DbContext
    {
        public appWikiContext()
        {
        }

        public appWikiContext(DbContextOptions<appWikiContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Artigo> Artigos { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {

                optionsBuilder.UseSqlite("Data Source=appWiki.db");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
