using System;

namespace DsEad.Models
{
    public class Artigo
    {
        public int Id { get; set; }
        public string Titulo { get; set; }
        public string Conteudo { get; set; }
        public DateTime CriadoEm { get; set; }
    }
}